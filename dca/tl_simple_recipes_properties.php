<?php

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2013 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    BBK (BilderBuchKino)
 * @license    commercial
 * @filesource
 */

/**
 * Table tl_bbk_properties
 */
$GLOBALS['TL_DCA']['tl_simple_recipes_properties'] = array
(

	// Config
	'config' => array
	(
		'dataContainer'               => 'Table',
		'enableVersioning'            => false,
		'onload_callback' => array
		(
			array('tl_simple_recipes_properties', 'create_property_entry')
		),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
			)
		)

	),
	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 1,
// 			'fields'                  => array('mediatyp'),
			'flag'                    => 12,
			'panelLayout'             => 'filter;search,limit'
		),
		'label' => array
		(
			'fields'                  => array('title', 'author'),
			'format'                  => '%s (%s)',
// 			'label_callback'          => array('tl_story_book_cinema', 'listEntries'),
		),
		'global_operations' => array
		(
			'properties' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['properties'],
				'href'                => 'table=tl_bbk_properties&act=edit',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			),
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset();" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['edit'],
				'href'                => 'table=tl_story_book_cinema&act=edit',
				'icon'                => 'edit.gif',
				'attributes'          => 'class="contextmenu"'
			),
			'editheader' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['editheader'],
				'href'                => 'act=edit',
				'icon'                => 'header.gif',
// 				'button_callback'     => array('story_book_cinema', 'editHeader'),
				'attributes'          => 'class="edit-header"'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['copy'],
				'href'                => 'act=copy',
				'icon'                => 'copy.gif'
			),

			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_story_book_cinema']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
			)

		)
	),

	// Palettes
	'palettes' => array
	(
		'default'                     => '{reader_legend},jumpTo',

	),

	// Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default '0'"
		),
		'modify' => array
		(
			'sql' => "int(10) unsigned NOT NULL default '0'"
		),
		'jumpTo' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_simple_recipes_properties']['jumpTo'],
			'exclude'                 => true,
			'inputType'               => 'pageTree',
			'foreignKey'              => 'tl_page.title',
			'eval'                    => array('mandatory'=>true, 'fieldType'=>'radio'),
			'sql'                     => "int(10) unsigned NOT NULL default '0'",
			'relation'                => array('type'=>'hasOne', 'load'=>'eager')
		),
	)
);


/**
 * Class tl_simple_recipes_properties
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Leo Feyer 2005-2012
 * @author     Leo Feyer <http://www.contao.org>
 * @package    Controller
 */
class tl_simple_recipes_properties extends Backend
{

	/**
	 * Import the back end user object
	 */
	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


	/**
	* create an entry if id=1 not exists
	* @return none
	*/
	public function create_property_entry()
	{
		$testObj = $this->Database->execute('SELECT * FROM `tl_simple_recipes_properties`');

		if($testObj->numRows == 0)
		{
			$this->Database->execute('INSERT INTO `tl_simple_recipes_properties`(`id`) VALUES(1)');
		}
	}




}

?>
