<?php
namespace Srhinow\SimpleRecipes\Models;
/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

use Contao\Model;

/**
 * Reads and writes Credit Items
 *
 * @property integer $id
 * @property integer $tstamp
 * @property string  $title
 *
 * @method static SimpleRecipesPropertiesModel|null findById($id, $opt=array())
 * @method static SimpleRecipesPropertiesModel|null findByIdOrAlias($val, $opt=array())
 * @method static SimpleRecipesPropertiesModel|null findByPk($id, $opt=array())
 * @method static SimpleRecipesPropertiesModel|null findOneBy($col, $val, $opt=array())
 * @method static SimpleRecipesPropertiesModel|null findOneByTstamp($val, $opt=array())
 * @method static SimpleRecipesPropertiesModel|null findOneByTitle($val, $opt=array())

 *
 * @method static \Model\Collection|SimpleRecipesPropertiesModel[]|SimpleRecipesPropertiesModel|null findByTstamp($val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesPropertiesModel[]|SimpleRecipesPropertiesModel|null findByTitle($val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesPropertiesModel[]|SimpleRecipesPropertiesModel|null findBy($col, $val, $opt=array())
 * @method static \Model\Collection|SimpleRecipesPropertiesModel[]|SimpleRecipesPropertiesModel|null findAll($opt=array())
 *
 * @method static integer countById($id, $opt=array())
 *
 * @author Leo Feyer <https://github.com/leofeyer>
 */
class SimpleRecipesPropertiesModel extends Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_simple_recipes_properties';

    /**
     * @param $varId
     * @param array $arrOptions
     * @return null|SimpleRecipesPropertiesModel
     */
	public static function findPropertiesById($varId, array $arrOptions=array())
	{
		$t = static::$strTable;

		return static::findByPk( $varId, $arrOptions);
	}	


}
