<?php
namespace Srhinow\SimpleRecipes\Hooks;

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */

use Contao\Frontend;
use Contao\PageModel;
use Srhinow\SimpleRecipes\Models\SimpleRecipesEntriesModel;
use Srhinow\SimpleRecipes\Models\SimpleRecipesPropertiesModel;

class SimpleRecipesHooks extends Frontend
{
	/**
	 * Add store items to the indexer
	 *
	 * @param array   $arrPages
	 * @param integer $intRoot
	 * @param boolean $blnIsSitemap
	 *
	 * @return array
	 */
	public function getSearchablePages($arrPages, $intRoot=0, $blnIsSitemap=false)
	{
        $arrPages = array();
        $strBase = '';


		$arrProcessed = array();
		$time = \Date::floorToMinute();

		$props = SimpleRecipesPropertiesModel::findPropertiesById(1);

		// Walk through each archive
		if ($props !== null)
		{
			// Skip if properties without target page
			if (!$props->jumpTo)
			{
				return $arrPages;
			}

			$itemsObj = SimpleRecipesEntriesModel::findRecipes();

			if($itemsObj !== null)
			{
				$objParent = PageModel::findWithDetails($props->jumpTo);

				// Set the domain (see #6421)
				$domain = ($objParent->rootUseSSL ? 'https://' : 'http://') . ($objParent->domain ?: \Environment::get('host')) . TL_PATH . '/';
				$strUrl = $domain . $this->generateFrontendUrl($objParent->row(), ((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ?  '/%s' : '/items/%s'), $objParent->language);

				while($itemsObj->next())
				{
					// Link to the default page
					$arrPages[] = $strBase . sprintf($strUrl, (($itemsObj->alias != '' && !\Config::get('disableAlias')) ? $itemsObj->alias : $itemsObj->id));
				}

			}
		}

		return $arrPages;
	}

}
