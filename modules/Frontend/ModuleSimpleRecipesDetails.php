<?php
namespace Srhinow\SimpleRecipes\Modules\Frontend;

/**
 * PHP version 5
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    simple_recipes
 * @license    LGPL
 * @filesource
 */
/**
 * Run in a custom namespace, so the class can be replaced
 */

use Contao\Database;
use Srhinow\SimpleRecipes\Models\SimpleRecipesCategoriesModel;
use Srhinow\SimpleRecipes\Models\SimpleRecipesEntriesModel;


/**
 * Class ModuleSimpleRecipesDetails
 *
 * Front end module "simple_recipes_details".
 */
class ModuleSimpleRecipesDetails extends ModuleSimpleRecipes
{
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_simple_recipes_details';

	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		if (TL_MODE == 'BE')
		{
			$objTemplate = new \BackendTemplate('be_wildcard');

			$objTemplate->wildcard = '### REZEPT-DETAILS ###';
			$objTemplate->title = $this->headline;
			$objTemplate->id = $this->id;
			$objTemplate->link = $this->name;
			$objTemplate->href = 'contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->id;

			return $objTemplate->parse();
		}

		// Set the item from the auto_item parameter
		if (!isset($_GET['recipe']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
		{
			\Input::setGet('recipe', \Input::get('auto_item'));
		}

		// Do not index or cache the page if no news item has been specified
		if (!\Input::get('recipe'))
		{
			global $objPage;
			$objPage->noSearch = 1;
			$objPage->cache = 0;
			return '';
		}

		return parent::generate();
	}


	/**
	 * Generate the module
	 */
	protected function compile()
	{

		global $objPage;

		// Get the total number of items
		$objRecipe = SimpleRecipesEntriesModel::findByIdOrAlias(\Input::get('recipe'));

		if ($objRecipe === null)
		{
			// Do not index or cache the page
			$objPage->noSearch = 1;
			$objPage->cache = 0;

			// Send a 404 header
			header('HTTP/1.1 404 Not Found');
			$this->Template->articles = '<p class="error">' . sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('items')) . '</p>';
			return;
		}

        $arrArticle = $this->parseRecipe($objRecipe);
        $this->Template->articles = $arrArticle;

        // Overwrite the page title (see #2853 and #4955)
        if ($objRecipe->headline != '')
        {
            $objPage->pageTitle = strip_tags(strip_insert_tags($objRecipe->headline));
        }

        // Overwrite the page description
        if ($objRecipe->teaser != '')
        {
            $objPage->description = $this->prepareMetaDescription($objRecipe->teaser);
        }




//		print_r($recipeData);


		$this->Template->referer = 'javascript:history.go(-1)';
		$this->Template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];
	}
}
